package pl.codementors.comics;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /**
     * Set of comics contained in the library.
     */
    private Set<Comic> comics = new HashSet<>();

    /**
     * @return All comics in the library. The returned collection is unmodifiable so it can not be changed
     * outside the library.
     */
    public Collection<Comic> getComics() {

        return Collections.unmodifiableSet(comics);
    }

    /**
     * Adds comic to the library. If comic is already in the library does nothing. If comic is null does nothing.
     *
     * @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (comic == null || comics.contains(comic)) {
            return;
        }
        comics.add(comic);
    }

    /**
     * Removes comic from the library. If comics is not present in the library does nothing.
     *
     * @param comic Comic to be removed from the library.
     */
    public void remove(Comic comic) {
        comics.remove(comic);

    }

    /**
     * Changes covers of all comics in the library.
     *
     * @param cover Cover type for all comics in the library.
     */
    public void changeCovers(Comic.Cover cover) {
        for (Comic c : comics) {
            c.setCover(cover);
        }

    }

    /**
     * @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getAuthors() {
        Set<String> authors = new TreeSet<>();
        for (Comic c : comics) {
            authors.add(c.getAuthor());
        }
        return Collections.unmodifiableSet(authors);

    }

    /**
     * @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.
     */
    public Collection<String> getSeries() {
        Set<String> series = new TreeSet<>();
        for (Comic c : comics) {
            series.add(c.getSeries());
        }
        return Collections.unmodifiableSet(series);


    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     * <p>
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     * <p>
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     * <p>
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {

        try (FileReader fileReader = new FileReader(file);
             Scanner reader = new Scanner(fileReader)) {
            int numberOfComics;
            numberOfComics = reader.nextInt();
            reader.skip("\n");
            for (int i = 0; i < numberOfComics; i++) {
                String title = reader.nextLine();
                String author = reader.nextLine();
                String series = reader.nextLine();
                Comic.Cover cover = Comic.Cover.valueOf(reader.next());
                int publishingMonth = reader.nextInt();
                int publishingYear = reader.nextInt();
                reader.skip("\n");
                Comic comic = new Comic(title, author, series, cover, publishingYear, publishingMonth);
                comics.add(comic);
            }


        } catch (IOException | NullPointerException | NoSuchElementException i) {
            //  i.printStackTrace();
        }
    }

    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */
    public int countBySeries(String series) {

        int numberOfSameSeries = 0;
        for (Comic c : comics) {
            if (c.getSeries().equals(series)) {
                numberOfSameSeries++;
            }
        }
        return numberOfSameSeries;
    }

    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {

        int numberOfSameAuthor = 0;
        for (Comic c : comics) {
            if (c.getAuthor().equals(author)) {
                numberOfSameAuthor++;
            }
        }
        return numberOfSameAuthor;
    }

    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {

        int numberOfSameYear = 0;
        for (Comic c : comics) {
            if (c.getPublishYear() == year) {
                numberOfSameYear++;
            }
        }
        return numberOfSameYear;
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year  Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {

        int numberOfSameYearAndMonth = 0;
        for (Comic c : comics) {
            if (c.getPublishYear() == year && c.getPublishMonth() == month) {
                numberOfSameYearAndMonth++;
            }
        }
        return numberOfSameYearAndMonth;
    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {

        for (Iterator<Comic> it = comics.iterator(); it.hasNext(); ) {
            if ((it.next().getPublishYear()) < year) {
                it.remove();
            }
        }
    }

    /**
     * Removes all comics with publish year larger than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided year.
     */
    public void removeAllNewerThan(int year) {

        for (Iterator<Comic> it = comics.iterator(); it.hasNext(); ) {
            if ((it.next().getPublishYear()) > year) {
                it.remove();
            }
        }
    }

    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {

        for (Iterator<Comic> it = comics.iterator(); it.hasNext(); ) {
            if (it.next().getAuthor().equals(author)) {
                it.remove();
            }
        }
    }

    /**
     * Removes all comics written in provided publish year and month. For the removal process method uses iterator.
     *
     * @param year  Provided year.
     * @param month Provided month.
     */
    public void removeAllFromYearAndMonth(int year, int month) {

        for (Iterator<Comic> it = comics.iterator(); it.hasNext(); ) {
            Comic comicForCheck = it.next();
            if ((comicForCheck.getPublishYear() == year) && (comicForCheck.getPublishMonth() == month)) {
                it.remove();
            }
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */
    public Map<String, Collection<Comic>> getAuthorsComics() {

        Map<String, Collection<Comic>> mapAuthorComic = new TreeMap<>();

        for (Comic c : comics) {
            if (mapAuthorComic.containsKey(c.getAuthor())) {

                Set<Comic> comicFromAuthor = (Set) mapAuthorComic.get(c.getAuthor());
                comicFromAuthor.add(c);

            } else {
                Set<Comic> comicFromAuthor = new HashSet<>();
                comicFromAuthor.add(c);
                String authorOfComic = c.getAuthor();
                mapAuthorComic.put(authorOfComic, comicFromAuthor);
            }
        }


        return mapAuthorComic;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Integer, Collection<Comic>> getYearsComics() {


        Map<Integer, Collection<Comic>> mapYearComic = new TreeMap<>();

        for (Comic c : comics) {
            if (mapYearComic.containsKey(c.getPublishYear())) {

                Set<Comic> comicFromYear = (Set) mapYearComic.get(c.getPublishYear());
                comicFromYear.add(c);

            } else {
                Set<Comic> comicFromYear = new HashSet<>();
                comicFromYear.add(c);
                Integer yearOfComic = c.getPublishYear();
                mapYearComic.put(yearOfComic, comicFromYear);
            }
        }


        return mapYearComic;
    }


    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish (year, month)->comics. Map keys are publish year and month (Pair of Integers, generics can not be simple types
     * so instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */
    public Map<Pair<Integer, Integer>, Collection<Comic>> getYearsMonthsComics() {


        Map<Pair<Integer, Integer>, Collection<Comic>> mapYearMonthComic = new TreeMap<>();
        for (Comic c : comics) {
            if (mapYearMonthComic.containsKey(new MutablePair<>(c.getPublishYear(), c.getPublishMonth()))) {

                Set<Comic> comicFromYearMonth = (Set) mapYearMonthComic.get(new MutablePair<>(c.getPublishYear(), c.getPublishMonth()));
                comicFromYearMonth.add(c);

            } else {
                Set<Comic> comicFromYear = new HashSet<>();
                comicFromYear.add(c);
                mapYearMonthComic.put(new MutablePair<>(c.getPublishYear(), c.getPublishMonth()), comicFromYear);
            }
        }


        return mapYearMonthComic;
    }


}
